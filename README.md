# starter

A program starter for the icon tray.

The program gets startet by a BAT file, which can be linked into the Windows Autostart folder.

During startup the program searches for a configuration file called `startet.json`, which 
contains the menu definition. The configuarion file has the follwoing syntax:
~~~json
{
  "menu": {
    "Windows": {
      "Explorer": ["explorer.exe"],
      "Terminal": ["cmd.exe", "/C", "start"]
    },
    "Tools": {
      "Firefox": ["C:\\Program Files\\Mozilla Firefox\\firefox.exe", "-P"]
    },
    "Development": {
      "Notepad++": ["C:\\Program Files\\Notepad++\\notepad++.exe"],
      "Emacs": ["C:\\Program Files\\Emacs\\emacs-28.2\\bin\\runemacs.exe"]
    },
    "Graphics": {
      "yEd": ["C:\\Program Files\\yWorks\\yEd\\yEd.exe"]
    }
  }
}
~~~
The nesting level of the menu definition is limited to two.  Commands a definied as
arrays.  The first element of each array is the program name.