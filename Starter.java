import com.google.gson.Gson;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import javax.swing.JOptionPane;

public class Starter
{
  static class Config {
    Map<String,Map<String,String[]>> menu;
  }

  static Image image ()
  {
    int height = 128;
    int width = height;
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics g = image.getGraphics();
    g.setColor(Color.RED);
    g.fillPolygon(new int[] {width, 0, 0}, new int[] {height / 2, 0, height}, 3);
    return image;
  }

  static Runnable starter(String[] command)
  {
    return () -> {
      try {
	new ProcessBuilder(command)
	  .directory(new File(System.getProperty("user.home")))
	  .inheritIO()
	  .start(); }
      catch (Throwable t) {
	JOptionPane.showMessageDialog(null, t.getMessage()); }};
  }

  static MenuItem item(String name, Runnable action)
  {
    MenuItem item = new MenuItem(name);
    item.addActionListener(new ActionListener() {
	@Override
	public void actionPerformed(ActionEvent ev) { action.run(); }});
    return item;
  }
  
  static PopupMenu menu (Map<String,Map<String,String[]>> tree)
  {
    PopupMenu popup = new PopupMenu();
    tree.forEach((section, nodes) -> {
	Menu menu = new Menu(section);
	popup.add (menu);
	nodes.forEach((node, command) -> {
	    menu.add(item(node, starter(command))); }); });
    popup.addSeparator();
    popup.add(item("Reload", () -> { load(); }));
    popup.add(item("Close", () -> {}));
    popup.add(item("Exit", () -> { System.exit(0); }));
    return popup;
  }

  static Config config()
  {
    String name = "starter.json";
    File cwd_config = new File(name);
    File home_config = new File(System.getProperty("user.home"), name);
    File file = null;
    if (home_config.exists() && home_config.canRead())
      file = home_config;
    else
      if (cwd_config.exists() && cwd_config.canRead())
	file = cwd_config;
      else
	throw new Error("Can not find config file: " + name);
    FileReader reader = null;
    try {
      reader = new FileReader(file); }
    catch (FileNotFoundException ex) {
      throw new Error("Can not read config file: " + file.toString(), ex); }
    Config config = new Gson().fromJson(reader, Config.class);
    return config;
  }

  static Config config = null;
  static Image image = null;
  static String title = "Starter";
  static TrayIcon icon = null;
  static PopupMenu menu = null;
  
  static void icon()
  {
    try {
      if (icon != null)
	SystemTray.getSystemTray().remove(icon);
      icon = new TrayIcon(image, title, menu);
      icon.setImageAutoSize(true);
      SystemTray.getSystemTray().add(icon); }
    catch (AWTException ex) {
      throw new Error("Can not update tray icon", ex); }
  }

  static void load()
  {
    config = config();
    menu = menu(config.menu);
    icon();
  }
  
  public static void main(String []args)
  {
    try {
      if (!SystemTray.isSupported())
	throw new UnsupportedOperationException("SystemTray is not supported.");
      image = image();
      load(); }
    catch (Throwable t) {
      t.printStackTrace (); }
  }
}
